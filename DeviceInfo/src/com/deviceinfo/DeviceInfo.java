
package com.deviceinfo;

import android.content.Context;

import com.device.info.R;
import com.deviceinfo.util.AbstractInfoUtil;
import com.deviceinfo.util.AndroidInfoUtil;
import com.deviceinfo.util.ConnectionInfoUtil;
import com.deviceinfo.util.Info;
import com.deviceinfo.util.ScreenInfoUtil;
import com.deviceinfo.util.SensorInfoUtil;
import com.deviceinfo.util.StorageInfoUtil;
import com.deviceinfo.util.TelephonyInfoUtil;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DeviceInfo {

    /**
     * Type of information for the device.
     */
    public enum InfoType {
        ANDROID, SCREEN, CONNECTION, STORAGE, TELEPHONY, SENSORS
    };

    private static final String TIMESTAMP_PARAMETER_KEY = "timestamp";

    private AndroidInfoUtil mAndroidInfoUtil;

    private ScreenInfoUtil mScreenInfoUtil;

    private ConnectionInfoUtil mConnectionInfoUtil;

    private StorageInfoUtil mStorageInfoUtil;

    private TelephonyInfoUtil mTelephonyInfoUtil;

    private SensorInfoUtil mSensorInfoUtil;

    private static DeviceInfo sInstance = null;

    private Context mContext;

    /**
     * Returns instance of this class. This method always returns the first
     * created object.<br/>
     * <b>Note:</b> Before using the methods of this class be sure it is initialized with
     * {@link #init(Context)} method.
     */
    public static DeviceInfo getInstance() {
        if (sInstance == null) {
            sInstance = new DeviceInfo();
        }
        return sInstance;
    }

    /**
     * Private constructor so new instances can not be made from other classes.
     */
    private DeviceInfo() {

    }

    /**
     * Initialize all info for the device.
     */
    public void init(Context context) {
        mContext = context;
        initInfoUtils(mContext);
    }

    /**
     * Refresh the values so if there are changes they take effect.
     */
    public void refreshValues(Context context) {
        mAndroidInfoUtil.refreshValues();
        mScreenInfoUtil.refreshValues();
        mConnectionInfoUtil.refreshValues();
        mStorageInfoUtil.refreshValues();
        mTelephonyInfoUtil.refreshValues();
        mSensorInfoUtil.refreshValues();
    }

    /**
     * Initializes all info utils
     */
    private void initInfoUtils(Context context) {
        mAndroidInfoUtil = AndroidInfoUtil.getInstance();
        mAndroidInfoUtil.init(context);

        mScreenInfoUtil = ScreenInfoUtil.getInstance();
        mScreenInfoUtil.init(context);

        mConnectionInfoUtil = ConnectionInfoUtil.getInstance();
        mConnectionInfoUtil.init(context);

        mStorageInfoUtil = StorageInfoUtil.getInstance();
        mStorageInfoUtil.init(context);

        mTelephonyInfoUtil = TelephonyInfoUtil.getInstance();
        mTelephonyInfoUtil.init(context);

        mSensorInfoUtil = SensorInfoUtil.getInstance();
        mSensorInfoUtil.init(context);
    }

    /**
     * Returns list of name-value pairs. This method throws {@link NullPointerException} if
     * {@link #init(Context)} method is not called after the creation of the instance.
     * 
     * @param type
     *            one of {@link InfoType#ANDROID}, {@link InfoType#SCREEN},
     *            {@link InfoType#CONNECTION}, {@link InfoType#STORAGE}, {@link InfoType#TELEPHONY},
     *            {@link InfoType#SENSORS}
     * @return list of {@link NameValuePair} objects.
     */
    public List<NameValuePair> getInfo(InfoType type) {
        List<NameValuePair> list = null;
        switch (type) {
            case ANDROID:
                list = getInfoAsList(mAndroidInfoUtil);
            case SCREEN:
                list = getInfoAsList(mScreenInfoUtil);
            case CONNECTION:
                list = getInfoAsList(mConnectionInfoUtil);
            case STORAGE:
                list = getInfoAsList(mStorageInfoUtil);
            case TELEPHONY:
                list = getInfoAsList(mTelephonyInfoUtil);
            case SENSORS:
                list = getInfoAsList(mSensorInfoUtil);
        }

        // Add timestamp no matter of the type of info
        if (list != null) {
            list.add(new BasicNameValuePair(TIMESTAMP_PARAMETER_KEY, getTime()));
        }

        return list;
    }

    /**
     * Returns {@link Info} object which is of one of {@link InfoType}s.
     * 
     * @param id as String. It has to be one of the ids in {@link R.string}. Resources for the
     *            different types start with
     *            different prefix:
     *            <table border="1">
     *            <tr>
     *            <th>{@link InfoType}</th>
     *            <th>Prefix</th>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#ANDROID}</td>
     *            <td>ai</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#SCREEN}</td>
     *            <td>si</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#CONNECTION}</td>
     *            <td>ci</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#STORAGE}</td>
     *            <td>sti</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#TELEPHONY}</td>
     *            <td>ti</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#SENSORS}</td>
     *            <td>sei</td>
     *            </tr>
     *            </table>
     *            The suffix has to be "_ID".
     * @param type one of {@link InfoType}s.
     * @return {@link Info} object for this id. If there is no info for it or it the type is wrong
     *         null
     */
    public Info getInfoForId(String id, InfoType type) {
        switch (type) {
            case ANDROID:
                return mAndroidInfoUtil.getInfoFor(id);
            case SCREEN:
                return mScreenInfoUtil.getInfoFor(id);
            case CONNECTION:
                return mConnectionInfoUtil.getInfoFor(id);
            case STORAGE:
                return mStorageInfoUtil.getInfoFor(id);
            case TELEPHONY:
                return mTelephonyInfoUtil.getInfoFor(id);
            case SENSORS:
                return mSensorInfoUtil.getInfoFor(id);
        }

        return null;
    }

    /**
     * Returns {@link Info} object which is of one of {@link InfoType}s.
     * 
     * @param id_R_string_id string resource for the id of this info. Resources for the different
     *            types start with
     *            different prefix:
     *            <table border="1">
     *            <tr>
     *            <th>{@link InfoType}</th>
     *            <th>Prefix</th>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#ANDROID}</td>
     *            <td>ai</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#SCREEN}</td>
     *            <td>si</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#CONNECTION}</td>
     *            <td>ci</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#STORAGE}</td>
     *            <td>sti</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#TELEPHONY}</td>
     *            <td>ti</td>
     *            </tr>
     *            <tr>
     *            <td>{@link InfoType#SENSORS}</td>
     *            <td>sei</td>
     *            </tr>
     *            </table>
     *            The suffix has to be "_ID".
     * @param type one of {@link InfoType}s.
     * @return {@link Info} object for this id. If there is no info for it or it the type is wrong
     *         null
     */
    public Info getInfoForId(int id_R_string_id, InfoType type) {
        return getInfoForId(mContext.getResources().getString(id_R_string_id), type);
    }

    /**
     * Same as {@link #getInfoForId(String, InfoType)} but returns the info as {@link NameValuePair}
     * .
     */
    public NameValuePair getInfoForIdAsPair(String id, InfoType type) {
        Info info = getInfoForId(id, type);
        return new BasicNameValuePair(info.getId(), info.getValue());
    }

    /**
     * Same as {@link #getInfoForId(int, InfoType)} but returns the info as {@link NameValuePair}.
     */
    public NameValuePair getInfoForIdAsPair(int id_R_string_id, InfoType type) {
        Info info = getInfoForId(id_R_string_id, type);
        return new BasicNameValuePair(info.getId(), info.getValue());
    }

    /**
     * Returns list of name-value pairs. This method throws {@link NullPointerException} if
     * {@link #init(Context)} method is not called after the creation of the instance.
     */
    public List<NameValuePair> getBaseInfo() {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        // Brand: text
        Info brand = mAndroidInfoUtil.getInfoFor(R.string.ai_Build_BRAND_ID);
        nameValuePairs.add(new BasicNameValuePair(brand.getId(), brand.getValue()));

        // Model: text
        Info model = mAndroidInfoUtil.getInfoFor(R.string.ai_Build_MODEL_ID);
        nameValuePairs.add(new BasicNameValuePair(model.getId(), model.getValue()));

        // OS: text
        Info os = mAndroidInfoUtil.getInfoFor(R.string.ai_OS_ID);
        nameValuePairs.add(new BasicNameValuePair(os.getId(), os.getValue()));

        // OS version: number
        Info osVersion = mAndroidInfoUtil.getInfoFor(R.string.ai_Build_VERSION_RELEASE_ID);
        nameValuePairs.add(new BasicNameValuePair(osVersion.getId(), osVersion.getValue()));

        // Resolution: text
        Info resolution = mScreenInfoUtil.getInfoFor(R.string.si_SCREEN_RESOLUTION_ID);
        nameValuePairs.add(new BasicNameValuePair(resolution.getId(), resolution.getValue()));

        // DPI: number
        Info dpi = mScreenInfoUtil.getInfoFor(R.string.si_SCREEN_DPI_ID);
        nameValuePairs.add(new BasicNameValuePair(dpi.getId(), dpi.getValue()));

        // Screen density: text
        Info screenDensity = mScreenInfoUtil.getInfoFor(R.string.si_SCREEN_DENSITY_ID);
        nameValuePairs.add(new BasicNameValuePair(screenDensity.getId(), screenDensity.getValue()));

        // CPU: text
        Info cpu = mAndroidInfoUtil.getInfoFor(R.string.ai_Build_CPU_ID);
        nameValuePairs.add(new BasicNameValuePair(cpu.getId(), cpu.getValue()));

        // Max RAM that can be used
        Info ram = mAndroidInfoUtil.getInfoFor(R.string.ai_RAM_MAX_ID);
        nameValuePairs.add(new BasicNameValuePair(ram.getId(), ram.getValue()));

        // Used RAM
        Info ramUsed = mAndroidInfoUtil.getInfoFor(R.string.ai_RAM_USED_ID);
        nameValuePairs.add(new BasicNameValuePair(ramUsed.getId(), ramUsed.getValue()));

        // Internal storage: number (MB)
        Info internalStorage = mStorageInfoUtil.getInfoFor(R.string.sti_INTERNAL_SIZE_TOTAL_MB_ID);
        nameValuePairs.add(new BasicNameValuePair(internalStorage.getId(), internalStorage
                .getValue()));

        // External storage: number (MB)
        Info externalStorage = mStorageInfoUtil.getInfoFor(R.string.sti_EXTERNAL_SIZE_TOTAL_MB_ID);
        nameValuePairs.add(new BasicNameValuePair(externalStorage.getId(), externalStorage
                .getValue()));

        // Time: timestamp
        nameValuePairs.add(new BasicNameValuePair(TIMESTAMP_PARAMETER_KEY, getTime()));

        // WiFi: boolean
        Info wifi = mConnectionInfoUtil.getInfoFor(R.string.ci_WIFI_CONNECTED_ID);
        nameValuePairs.add(new BasicNameValuePair(wifi.getId(), wifi.getValue()));

        // Is 3G module available
        Info cell = mConnectionInfoUtil.getInfoFor(R.string.ci_3G_AVAILABLE_ID);
        nameValuePairs.add(new BasicNameValuePair(cell.getId(), cell.getValue()));

        // TODO Add the parameter when GCM module is ready
        // RegID (from GCM): text
        // String regID = Preferences.getGcmRegId();
        // nameValuePairs.add(new BasicNameValuePair("reg_id", regID);

        return nameValuePairs;
    }

    private List<NameValuePair> getInfoAsList(AbstractInfoUtil infoUtil) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        String[] infoIds = infoUtil.getInfoIDs();
        for (int i = 0; i < infoIds.length; i++) {
            Info info = infoUtil.getInfoFor(infoIds[i]);

            NameValuePair nameValuePair = new BasicNameValuePair(info.getId(), info.getValue());
            nameValuePairs.add(nameValuePair);
        }

        return nameValuePairs;
    }

    /**
     * Get time in milliseconds as string
     */
    private String getTime() {
        return String.valueOf(new Date().getTime());
    }

}

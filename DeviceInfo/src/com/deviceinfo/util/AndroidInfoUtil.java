
package com.deviceinfo.util;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import com.device.info.R;

public final class AndroidInfoUtil extends AbstractInfoUtil {

    private static AndroidInfoUtil instance = null;

    public static AndroidInfoUtil getInstance() {
        if (instance == null) {
            instance = new AndroidInfoUtil();
        }
        return instance;
    }

    private AndroidInfoUtil() {
    }

    protected void initIDs() {
        addInfo(R.string.ai_ANDROID_ID_ID, Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID), R.string.ai_ANDROID_ID);
        addInfo(R.string.ai_OS_ID, "android", R.string.ai_OS);
        addInfo(R.string.ai_Build_BOARD_ID, Build.BOARD, R.string.ai_Build_BOARD);
        addInfo(R.string.ai_Build_BOOTLOADER_ID, Build.BOOTLOADER, R.string.ai_Build_BOOTLOADER);
        addInfo(R.string.ai_Build_BRAND_ID, Build.BRAND, R.string.ai_Build_BRAND);
        addInfo(R.string.ai_Build_CPU_ID, Build.CPU_ABI, R.string.ai_Build_CPU);
        addInfo(R.string.ai_Build_CPU_ABI_ID, Build.CPU_ABI, R.string.ai_Build_CPU_ABI);
        addInfo(R.string.ai_Build_CPU_ABI2_ID, Build.CPU_ABI2, R.string.ai_Build_CPU_ABI2);
        addInfo(R.string.ai_Build_DEVICE_ID, Build.DEVICE, R.string.ai_Build_DEVICE);
        addInfo(R.string.ai_Build_DISPLAY_ID, Build.DISPLAY, R.string.ai_Build_DISPLAY);
        addInfo(R.string.ai_Build_FINGERPRINT_ID, Build.FINGERPRINT, R.string.ai_Build_FINGERPRINT);
        addInfo(R.string.ai_Build_HARDWARE_ID, Build.HARDWARE, R.string.ai_Build_HARDWARE);
        addInfo(R.string.ai_Build_HOST_ID, Build.HOST, R.string.ai_Build_HOST);
        addInfo(R.string.ai_Build_ID_ID, Build.ID, R.string.ai_Build_ID);
        addInfo(R.string.ai_Build_MANUFACTURER_ID, Build.MANUFACTURER,
                R.string.ai_Build_MANUFACTURER);
        addInfo(R.string.ai_Build_MODEL_ID, Build.MODEL, R.string.ai_Build_MODEL);
        addInfo(R.string.ai_Build_PRODUCT_ID, Build.PRODUCT, R.string.ai_Build_PRODUCT);
        addInfo(R.string.ai_Build_RADIO_ID, Build.RADIO, R.string.ai_Build_RADIO);
        addInfo(R.string.ai_Build_SERIAL_ID, Build.SERIAL, R.string.ai_Build_SERIAL);
        addInfo(R.string.ai_Build_TAGS_ID, Build.TAGS, R.string.ai_Build_TAGS);
        addInfo(R.string.ai_Build_TIME_ID, String.valueOf(Build.TIME), R.string.ai_Build_TIME);
        addInfo(R.string.ai_Build_TYPE_ID, Build.TYPE, R.string.ai_Build_TYPE);
        addInfo(R.string.ai_Build_UNKNOWN_ID, Build.UNKNOWN, R.string.ai_Build_UNKNOWN);
        addInfo(R.string.ai_Build_USER_ID, Build.USER, R.string.ai_Build_USER);
        addInfo(R.string.ai_Build_VERSION_CODENAME_ID, Build.VERSION.CODENAME,
                R.string.ai_Build_VERSION_CODENAME);
        addInfo(R.string.ai_Build_VERSION_INCREMENTAL_ID, Build.VERSION.INCREMENTAL,
                R.string.ai_Build_VERSION_INCREMENTAL);
        addInfo(R.string.ai_Build_VERSION_RELEASE_ID, Build.VERSION.RELEASE,
                R.string.ai_Build_VERSION_RELEASE);
        addInfo(R.string.ai_Build_VERSION_SDK_ID, Build.VERSION.SDK, R.string.ai_Build_VERSION_SDK);
        addInfo(R.string.ai_Build_VERSION_SDK_INT_ID, String.valueOf(Build.VERSION.SDK_INT),
                R.string.ai_Build_VERSION_SDK_INT);
        addInfo(R.string.ai_RAM_MAX_ID,
                String.valueOf(bytesToMBs(Runtime.getRuntime().maxMemory())), R.string.ai_RAM_MAX);
        addInfo(R.string.ai_RAM_USED_ID,
                String.valueOf(bytesToMBs(Runtime.getRuntime().totalMemory())),
                R.string.ai_RAM_USED);

        final PackageManager packageManager = mContext.getPackageManager();
        final FeatureInfo[] featuresList = packageManager.getSystemAvailableFeatures();
        String f = "";
        for (FeatureInfo feature : featuresList) {
            if (feature != null && feature.name != null) {
                f += feature.name + "\n";
            }
        }
        addInfo(R.string.ai_SYSTEM_AVAILABLE_FEATURES_ID, f, R.string.ai_SYSTEM_AVAILABLE_FEATURES);
    }

    public final boolean isFeatureAvailable(String feature) {
        final PackageManager packageManager = mContext.getPackageManager();
        final FeatureInfo[] featuresList = packageManager.getSystemAvailableFeatures();
        for (FeatureInfo f : featuresList) {
            if (f.name != null && f.name.equals(feature)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Converts bytes to megabytes
     * 
     * @param bytes
     * @return bytes converted
     */
    private static long bytesToMBs(long bytes) {
        return bytes / (1024 * 1024);
    }

}

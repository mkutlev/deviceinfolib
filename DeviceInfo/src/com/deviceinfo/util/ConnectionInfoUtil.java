
package com.deviceinfo.util;

import com.device.info.R;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class ConnectionInfoUtil extends AbstractInfoUtil {

    private static ConnectionInfoUtil instance = null;

    public static ConnectionInfoUtil getInstance() {
        if (instance == null) {
            instance = new ConnectionInfoUtil();
        }
        return instance;
    }

    private ConnectionInfoUtil() {
    }

    protected void initIDs() {

        ConnectivityManager connManager = (ConnectivityManager)mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager == null) {
            addInfo(R.string.ci_CONNECTION_MANAGER_ID, "Not supported",
                    R.string.ci_CONNECTION_MANAGER);
            addInfo(R.string.ci_WIFI_CONNECTED_ID, String.valueOf(false),
                    R.string.ci_WIFI_CONNECTED);
            addInfo(R.string.ci_3G_AVAILABLE_ID, String.valueOf(false), R.string.ci_3G_AVAILABLE);
            addInfo(R.string.ci_3G_CONNECTED_ID, String.valueOf(false), R.string.ci_3G_CONNECTED);

        } else {
            NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo == null) {
                addInfo(R.string.ci_NETWORK_INFO_WIFI_ID, "Not supported",
                        R.string.ci_NETWORK_INFO_WIFI);
                addInfo(R.string.ci_WIFI_CONNECTED_ID, String.valueOf(false),
                        R.string.ci_WIFI_CONNECTED);
            } else {
                addInfo(R.string.ci_NETWORK_INFO_WIFI_ID, networkInfo.isConnected() + "/"
                        + networkInfo.getTypeName() + "/" + networkInfo.getSubtypeName() + "/"
                        + networkInfo.getExtraInfo(), R.string.ci_NETWORK_INFO_WIFI);
                addInfo(R.string.ci_WIFI_CONNECTED_ID, String.valueOf(networkInfo.isConnected()),
                        R.string.ci_WIFI_CONNECTED);
            }

            NetworkInfo networkInfo3G = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (networkInfo3G == null) {
                addInfo(R.string.ci_3G_AVAILABLE_ID, String.valueOf(false),
                        R.string.ci_3G_AVAILABLE);
                addInfo(R.string.ci_3G_CONNECTED_ID, String.valueOf(false),
                        R.string.ci_3G_CONNECTED);
            } else {
                addInfo(R.string.ci_3G_AVAILABLE_ID, String.valueOf(true), R.string.ci_3G_AVAILABLE);
                addInfo(R.string.ci_3G_CONNECTED_ID, String.valueOf(networkInfo3G.isConnected()),
                        R.string.ci_3G_CONNECTED);
            }
        }

        WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo != null) {

                // Return the basic service set identifier (BSSID) of the current access point. The
                // BSSID may be null if there is no network currently connected.
                addInfo(R.string.ci_WIFI_BSSID_ID, wifiInfo.getBSSID(), R.string.ci_WIFI_BSSID);

                // true if this network does not broadcast its SSID, so an SSID-specific probe
                // request must be used for scans
                addInfo(R.string.ci_WIFI_HIDDEN_SSID_ID, String.valueOf(wifiInfo.getHiddenSSID()),
                        R.string.ci_WIFI_HIDDEN_SSID);

                // IP Address
                int ip = wifiInfo.getIpAddress();
                String ipString = String.format("%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff), (ip >> 24 & 0xff));
                addInfo(R.string.ci_WIFI_IP_ADDRESS_ID, ipString, R.string.ci_WIFI_IP_ADDRESS);

                // Returns the current link speed in LINK_SPEED_UNITS.
                addInfo(R.string.ci_WIFI_LINK_SPEED_ID, String.valueOf(wifiInfo.getLinkSpeed()),
                        R.string.ci_WIFI_LINK_SPEED);

                // Returns the MacAddress
                addInfo(R.string.ci_WIFI_MAC_ADDRESS_ID, wifiInfo.getMacAddress(),
                        R.string.ci_WIFI_MAC_ADDRESS);

                // Each configured network has a unique small integer ID, used to identify the
                // network when performing operations on the supplicant. This method returns the ID
                // for the currently connected network.
                addInfo(R.string.ci_WIFI_NETWORK_ID_ID, String.valueOf(wifiInfo.getNetworkId()),
                        R.string.ci_WIFI_NETWORK_ID);

                // Returns the received signal strength indicator of the current 802.11 network.
                addInfo(R.string.ci_WIFI_RSSI_ID, String.valueOf(wifiInfo.getRssi()),
                        R.string.ci_WIFI_RSSI);

                // Returns the service set identifier (SSID) of the current 802.11 network. If the
                // SSID can be decoded as UTF-8, it will be returned surrounded by double quotation
                // marks. Otherwise, it is returned as a string of hex digits. The SSID may be null
                // if there is no network currently connected.
                addInfo(R.string.ci_WIFI_SSID_ID, wifiInfo.getSSID(), R.string.ci_WIFI_SSID);

                // SupplicantState ss = wifiInfo.getSupplicantState();

            } else {
                addInfo(R.string.ci_WIFI_INFO_ID, "Not supported", R.string.ci_WIFI_INFO);
            }
        } else {
            addInfo(R.string.ci_WIFI_MANAGER_ID, "Not supported", R.string.ci_WIFI_MANAGER);
        }

    }

}

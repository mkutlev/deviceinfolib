
package com.deviceinfo.util;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;

import com.device.info.R;

import java.util.List;

public class SensorInfoUtil extends AbstractInfoUtil {

    private static SensorInfoUtil instance = null;

    public static SensorInfoUtil getInstance() {
        if (instance == null) {
            instance = new SensorInfoUtil();
        }
        return instance;
    }

    private SensorInfoUtil() {
    }

    private void addSensorInfo(int sensorType, String sensorName, int r_id_count, int r_id_info) {
        SensorManager sensorManager = (SensorManager)mContext
                .getSystemService(Context.SENSOR_SERVICE);

        List<Sensor> sensors = sensorManager.getSensorList(sensorType);

        if (sensors != null && !sensors.isEmpty()) {
            addInfo(sensorName + "_count", String.valueOf(sensors.size()), r_id_count);
            for (int i = 0; i < sensors.size(); i++) {
                Sensor sensor = sensors.get(i);
                String sensorInfo = "maximum_range: " + sensor.getMaximumRange() + ";\n"
                        + "min_delay: " + sensor.getMinDelay() + ";\n" + "name: "
                        + sensor.getName() + ";\n" + "power: " + sensor.getPower() + ";\n"
                        + "resolution: " + sensor.getResolution() + ";\n" + "type: "
                        + sensor.getType() + ";\n" + "vendor: " + sensor.getVendor() + ";\n"
                        + "version: " + sensor.getVersion() + ";";
                addInfo(sensorName + "_" + (i + 1) + "_info", sensorInfo, r_id_info);
            }

        } else {
            addInfo(sensorName + "_count", "0", r_id_count);
        }
    }

    private void addSensorInfo(int sensorType, int r_id_sensorName, int r_id_count, int r_id_info) {
        addSensorInfo(sensorType, mContext.getResources().getString(r_id_sensorName), r_id_count,
                r_id_info);
    }

    protected void initIDs() {

        addSensorInfo(Sensor.TYPE_ACCELEROMETER, R.string.sei_ACCELEROMETER,
                R.string.sei_ACCELEROMETER_COUNT, R.string.sei_ACCELEROMETER_INFO);
        addSensorInfo(Sensor.TYPE_GRAVITY, R.string.sei_GRAVITY, R.string.sei_GRAVITY_COUNT,
                R.string.sei_GRAVITY_INFO);
        addSensorInfo(Sensor.TYPE_GYROSCOPE, R.string.sei_GYROSCOPE, R.string.sei_GYROSCOPE_COUNT,
                R.string.sei_GYROSCOPE_INFO);
        addSensorInfo(Sensor.TYPE_LIGHT, R.string.sei_LIGHT, R.string.sei_LIGHT_COUNT,
                R.string.sei_LIGHT_INFO);
        addSensorInfo(Sensor.TYPE_LINEAR_ACCELERATION, R.string.sei_LINEAR_ACCELERATION,
                R.string.sei_LINEAR_ACCELERATION_COUNT, R.string.sei_LINEAR_ACCELERATION_INFO);
        addSensorInfo(Sensor.TYPE_MAGNETIC_FIELD, R.string.sei_MAGNETIC_FIELD,
                R.string.sei_MAGNETIC_FIELD_COUNT, R.string.sei_MAGNETIC_FIELD_INFO);
        addSensorInfo(Sensor.TYPE_ORIENTATION, R.string.sei_ORIENTATION,
                R.string.sei_ORIENTATION_COUNT, R.string.sei_ORIENTATION_INFO);
        addSensorInfo(Sensor.TYPE_PRESSURE, R.string.sei_PRESSURE, R.string.sei_PRESSURE_COUNT,
                R.string.sei_PRESSURE_INFO);
        addSensorInfo(Sensor.TYPE_PROXIMITY, R.string.sei_PROXIMITY, R.string.sei_PROXIMITY_COUNT,
                R.string.sei_PROXIMITY_INFO);
        addSensorInfo(Sensor.TYPE_ROTATION_VECTOR, R.string.sei_ROTATION_VECTOR,
                R.string.sei_ROTATION_VECTOR_COUNT, R.string.sei_ROTATION_VECTOR_INFO);
        addSensorInfo(Sensor.TYPE_TEMPERATURE, R.string.sei_TEMPERATURE,
                R.string.sei_TEMPERATURE_COUNT, R.string.sei_TEMPERATURE_INFO);

        LocationManager locationManager = (LocationManager)mContext
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            addInfo(R.string.sei_LOCATION_MANAGER_ID, "null", R.string.sei_LOCATION_MANAGER);
        } else {
            List<String> providers = locationManager.getAllProviders();
            if (providers == null || providers.isEmpty()) {
                addInfo(R.string.sei_LOCATION_PROVIDERS_ID, "none", R.string.sei_LOCATION_PROVIDERS);
            } else {
                String strProviders = "";
                for (String p : providers) {
                    strProviders += p + ";\n";
                }
                addInfo(R.string.sei_LOCATION_PROVIDERS_ID, strProviders,
                        R.string.sei_LOCATION_PROVIDERS);
            }
        }

        int camCount = Camera.getNumberOfCameras();
        addInfo(R.string.sei_CAMERA_COUNT_ID, String.valueOf(camCount), R.string.sei_CAMERA_COUNT);

        for (int i = 0; i < camCount; i++) {
            CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);

            String camFacing = "";
            switch (info.facing) {
                case CameraInfo.CAMERA_FACING_BACK:
                    camFacing = "BACK";
                    break;
                case CameraInfo.CAMERA_FACING_FRONT:
                    camFacing = "FRONT";
                    break;
                default:
                    camFacing = info.facing + " (Not supported)";
            }

            addInfo("camera_" + (i + 1) + "_facing", camFacing, R.string.sei_CAMERA_FACING);

            Camera camera = Camera.open(i);
            String cameraInfo = camera.getParameters().flatten();
            if (cameraInfo != null) {
                cameraInfo = cameraInfo.replace(";", ";\n");
            }

            addInfo("camera_" + (i + 1) + "_info", cameraInfo, R.string.sei_CAMERA_INFO);

            camera.release();
        }

    }

}

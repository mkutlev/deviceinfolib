
package com.deviceinfo.util;

import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;

public abstract class AbstractInfoUtil {

    protected Context mContext;

    protected Map<String, Info> mapIDs = new LinkedHashMap<String, Info>();

    public Info getInfoFor(String id) {
        return mapIDs.get(id);
    }

    public Info getInfoFor(int id_R_string_id) {
        return getInfoFor(mContext.getResources().getString(id_R_string_id));
    }

    public void init(Context context) {
        mContext = context;
        initIDs();
    }

    public void refreshValues() {
        mapIDs.clear();
        initIDs();
    }

    protected abstract void initIDs();

    protected void addInfo(String id, String value, int description_R_string_id) {
        mapIDs.put(id,
                new Info(id, value, mContext.getResources().getString(description_R_string_id)));
    }

    protected void addInfo(int id_R_string_id, String value, int description_R_string_id) {
        mapIDs.put(mContext.getResources().getString(id_R_string_id), new Info(mContext
                .getResources().getString(id_R_string_id), value, mContext.getResources()
                .getString(description_R_string_id)));
    }

    public String[] getInfoIDs() {
        return mapIDs.keySet().toArray(new String[0]);
    }

}

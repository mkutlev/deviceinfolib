
package com.deviceinfo.util;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;

import com.device.info.R;

import java.util.List;

public class TelephonyInfoUtil extends AbstractInfoUtil {

    private static TelephonyInfoUtil instance = null;

    public static TelephonyInfoUtil getInstance() {
        if (instance == null) {
            instance = new TelephonyInfoUtil();
        }
        return instance;
    }

    private TelephonyInfoUtil() {
    }

    public boolean isTelephonySupported() {
        return (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE) != null;
    }

    protected void initIDs() {
        TelephonyManager tm = (TelephonyManager)mContext
                .getSystemService(Context.TELEPHONY_SERVICE);

        if (!isTelephonySupported()) {
            addInfo(R.string.ti_TELEPHONY_MANAGER_ID, "Not supported",
                    R.string.ti_TELEPHONY_MANAGER);
            return;
        }

        // TODO listener???
        String callState = "";
        switch (tm.getCallState()) { // Returns a constant indicating the call state (cellular) on
                                     // the device.
            case TelephonyManager.CALL_STATE_IDLE:
                callState = "Idle";
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                callState = "Offhook";
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                callState = "Ringing";
                break;
            default:
                callState = tm.getCallState() + " (Not supported)";
        }
        addInfo(R.string.ti_CALL_STATE_ID, callState, R.string.ti_CALL_STATE);

        // TODO cells
        CellLocation cl = tm.getCellLocation(); // Returns the current location of the device.
                                                // Return null if current location is not available.
                                                // Requires Permission: ACCESS_COARSE_LOCATION or
                                                // ACCESS_FINE_LOCATION.
        String clInfo = (cl != null) ? cl.toString() : "null";
        addInfo(R.string.ti_CELL_LOCATION_ID, clInfo, R.string.ti_CELL_LOCATION);

        // TODO listener???
        String dataActivity = "";
        switch (tm.getDataActivity()) { // Returns a constant indicating the type of activity on a
                                        // data connection (cellular).
            case TelephonyManager.DATA_ACTIVITY_DORMANT:
                dataActivity = "Dormant";
                break;
            case TelephonyManager.DATA_ACTIVITY_IN:
                dataActivity = "In";
                break;
            case TelephonyManager.DATA_ACTIVITY_INOUT:
                dataActivity = "InOut";
                break;
            case TelephonyManager.DATA_ACTIVITY_NONE:
                dataActivity = "None";
                break;
            case TelephonyManager.DATA_ACTIVITY_OUT:
                dataActivity = "Out";
                break;
            default:
                dataActivity = tm.getDataActivity() + " (Not supported)";
        }
        addInfo(R.string.ti_DATA_ACTIVITY_ID, dataActivity, R.string.ti_DATA_ACTIVITY);

        // TODO listener???
        String dataState = "";
        switch (tm.getDataState()) { // Returns a constant indicating the current data connection
                                     // state (cellular).
            case TelephonyManager.DATA_CONNECTED:
                dataState = "Connected";
                break;
            case TelephonyManager.DATA_CONNECTING:
                dataState = "Connecting";
                break;
            case TelephonyManager.DATA_DISCONNECTED:
                dataState = "Disconnected";
                break;
            case TelephonyManager.DATA_SUSPENDED:
                dataState = "Suspended";
                break;
            default:
                dataState = tm.getDataState() + " (Not supported)";
        }
        addInfo(R.string.ti_DATA_STATE_ID, dataState, R.string.ti_DATA_STATE);

        // Returns the unique device ID, for example, the IMEI for GSM and the MEID or ESN for CDMA
        // phones. Return null if device ID is not available. Requires Permission: READ_PHONE_STATE
        addInfo(R.string.ti_DEVICE_ID_ID, tm.getDeviceId(), R.string.ti_DEVICE_ID);

        // Returns the software version number for the device, for example, the IMEI/SV for GSM
        // phones. Return null if the software version is not available. Requires Permission:
        // READ_PHONE_STATE
        addInfo(R.string.ti_DEVICE_SOFTWARE_VERSION_ID, tm.getDeviceSoftwareVersion(),
                R.string.ti_DEVICE_SOFTWARE_VERSION);

        // Returns the phone number string for line 1, for example, the MSISDN for a GSM phone.
        // Return null if it is unavailable. Requires Permission: READ_PHONE_STATE
        addInfo(R.string.ti_LINE1_NUMBER_ID, tm.getLine1Number(), R.string.ti_LINE1_NUMBER);

        // TODO cell info
        // Returns the neighboring cell information of the device. Requires Permission:
        // ACCESS_COARSE_UPDATES for NeighboringCellInfo
        List<NeighboringCellInfo> cellList = tm.getNeighboringCellInfo();
        String nci = "";
        if (cellList != null) {
            for (NeighboringCellInfo neighboringCellInfo : cellList) {
                nci += neighboringCellInfo.toString() + "\n";
            }
        } else {
            nci = "Unknown";
        }
        addInfo(R.string.ti_NEIGHBORING_CELL_INFO_ID, nci, R.string.ti_NEIGHBORING_CELL_INFO);

        // Returns the ISO country code equivalent of the current registered operator's MCC (Mobile
        // Country Code).
        addInfo(R.string.ti_NETWORK_COUNTRY_ISO_ID, tm.getNetworkCountryIso(),
                R.string.ti_NETWORK_COUNTRY_ISO);

        // Returns the numeric name (MCC+MNC) of current registered operator.
        addInfo(R.string.ti_NETWORK_OPERATOR_ID, tm.getNetworkOperator(),
                R.string.ti_NETWORK_OPERATOR);

        // Returns the alphabetic name of current registered operator.
        addInfo(R.string.ti_NETWORK_OPERATOR_NAME_ID, tm.getNetworkOperatorName(),
                R.string.ti_NETWORK_OPERATOR_NAME);

        // TODO listener???
        String networkType = "";
        switch (tm.getNetworkType()) { // Returns a constant indicating the radio technology
                                       // (network type) currently in use on the device for data
                                       // transmission.
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                networkType = "1xRTT";
                break;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                networkType = "CMDA";
                break;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                networkType = "EDGE";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                networkType = "EVDO_0";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                networkType = "EVDO_A";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                networkType = "EVDO_B";
                break;
            case TelephonyManager.NETWORK_TYPE_GPRS:
                networkType = "GPRS";
                break;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                networkType = "HSDPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                networkType = "HSPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                networkType = "HSUPA";
                break;
            case TelephonyManager.NETWORK_TYPE_IDEN:
                networkType = "IDEN";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                networkType = "UMTS";
                break;
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                networkType = "Unknown";
                break;
            default:
                networkType = tm.getNetworkType() + " (Not supported)";
        }
        addInfo(R.string.ti_NETWORK_TYPE_ID, networkType, R.string.ti_NETWORK_TYPE);

        // Returns a constant indicating the device phone type. This indicates the type of radio
        // used to transmit voice calls.
        String phoneType = "";
        switch (tm.getPhoneType()) { // Returns a constant indicating the current data connection
                                     // state (cellular).
            case TelephonyManager.PHONE_TYPE_NONE:
                phoneType = "None";
                break;
            case TelephonyManager.PHONE_TYPE_GSM:
                phoneType = "GSM";
                break;
            case TelephonyManager.PHONE_TYPE_CDMA:
                phoneType = "CDMA";
                break;
            case 3 /* TelephonyManager.PHONE_TYPE_SIP */:
                phoneType = "SIP";
                break;
            default:
                phoneType = tm.getPhoneType() + " (Not supported)";
        }
        addInfo(R.string.ti_PHONE_TYPE_ID, phoneType, R.string.ti_PHONE_TYPE);

        // Returns the ISO country code equivalent for the SIM provider's country code.
        addInfo(R.string.ti_SIM_COUNTRY_ISO_ID, tm.getSimCountryIso(), R.string.ti_SIM_COUNTRY_ISO);

        // Returns the MCC+MNC (mobile country code + mobile network code) of the provider of the
        // SIM. 5 or 6 decimal digits. SIM state must be SIM_STATE_READY
        addInfo(R.string.ti_SIM_OPERATOR_ID, tm.getSimOperator(), R.string.ti_SIM_OPERATOR);

        // Returns the Service Provider Name (SPN). SIM state must be SIM_STATE_READY
        addInfo(R.string.ti_SIM_OPERATOR_NAME_ID, tm.getSimOperatorName(),
                R.string.ti_SIM_OPERATOR_NAME);

        // Returns the serial number of the SIM, if applicable. Return null if it is unavailable.
        // Requires Permission: READ_PHONE_STATE
        addInfo(R.string.ti_SIM_SERIAL_NUMBER_ID, tm.getSimSerialNumber(),
                R.string.ti_SIM_SERIAL_NUMBER);

        // TODO listener???
        String simState = "";
        switch (tm.getSimState()) { // Returns a constant indicating the state of the device SIM
                                    // card.
            case TelephonyManager.SIM_STATE_ABSENT:
                simState = "Absent";
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                simState = "Network Locked";
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                simState = "PIN Required";
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                simState = "PUK Required";
                break;
            case TelephonyManager.SIM_STATE_READY:
                simState = "Ready";
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                simState = "Unknown";
                break;
            default:
                simState = tm.getSimState() + " (Not supported)";
        }
        addInfo(R.string.ti_SIM_STATE_ID, simState, R.string.ti_SIM_STATE);

        // Returns the unique subscriber ID, for example, the IMSI for a GSM phone. Return null if
        // it is unavailable. Requires Permission: READ_PHONE_STATE
        addInfo(R.string.ti_SUBSCRIBER_ID_ID, tm.getSubscriberId(), R.string.ti_SUBSCRIBER_ID);

        // Retrieves the alphabetic identifier associated with the voice mail number. Requires
        // Permission: READ_PHONE_STATE
        addInfo(R.string.ti_VOICE_MAIL_ALPHA_TAG_ID, tm.getVoiceMailAlphaTag(),
                R.string.ti_VOICE_MAIL_ALPHA_TAG);

        // Returns the voice mail number. Return null if it is unavailable. Requires Permission:
        // READ_PHONE_STATE
        addInfo(R.string.ti_VOICE_MAIL_NUMBER_ID, tm.getVoiceMailNumber(),
                R.string.ti_VOICE_MAIL_NUMBER);

    }
}

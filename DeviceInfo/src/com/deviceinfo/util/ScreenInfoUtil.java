
package com.deviceinfo.util;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

import com.device.info.R;

public class ScreenInfoUtil extends AbstractInfoUtil {

    private static ScreenInfoUtil instance = null;

    public static ScreenInfoUtil getInstance() {
        if (instance == null) {
            instance = new ScreenInfoUtil();
        }
        return instance;
    }

    private ScreenInfoUtil() {
    }

    private int NATURAL_ORIENTATION = Integer.MIN_VALUE;

    @Override
    protected void initIDs() {

        DisplayMetrics displayMetrics = new DisplayMetrics();

        WindowManager wm = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(displayMetrics);

        addInfo(R.string.si_SCREEN_DISPLAY_ID_ID, String.valueOf(display.getDisplayId()),
                R.string.si_SCREEN_DISPLAY_ID);

        Configuration config = mContext.getResources().getConfiguration();
        String layoutSize = "";
        int intLayoutSize = config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (intLayoutSize) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                layoutSize = "Small";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                layoutSize = "Normal";
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                layoutSize = "Large";
                break;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                layoutSize = "X-Large";
                break;
            case Configuration.SCREENLAYOUT_SIZE_UNDEFINED:
                layoutSize = "Undefined";
                break;
            default:
                layoutSize = intLayoutSize + " (Not supported)";
        }

        addInfo(R.string.si_SCREEN_LAYOUT_SIZE_ID, layoutSize, R.string.si_SCREEN_LAYOUT_SIZE);

        String layoutLong = "";
        int intLayoutLong = config.screenLayout & Configuration.SCREENLAYOUT_LONG_MASK;
        switch (intLayoutLong) {
            case Configuration.SCREENLAYOUT_LONG_NO:
                layoutLong = "Not Long";
                break;
            case Configuration.SCREENLAYOUT_LONG_YES:
                layoutLong = "Long";
                break;
            case Configuration.SCREENLAYOUT_LONG_UNDEFINED:
                layoutLong = "Undefined";
                break;
            default:
                layoutLong = intLayoutLong + " (Not supported)";
        }

        addInfo(R.string.si_SCREEN_LAYOUT_LONG_ID, layoutLong, R.string.si_SCREEN_LAYOUT_LONG);
        // Resolution in format 320x480
        addInfo(R.string.si_SCREEN_RESOLUTION_ID,
                String.valueOf(displayMetrics.widthPixels + "x" + displayMetrics.heightPixels),
                R.string.si_SCREEN_RESOLUTION);
        addInfo(R.string.si_SCREEN_WIDTH_PX_ID, String.valueOf(displayMetrics.widthPixels),
                R.string.si_SCREEN_WIDTH_PX);
        addInfo(R.string.si_SCREEN_HEIGHT_PX_ID, String.valueOf(displayMetrics.heightPixels),
                R.string.si_SCREEN_HEIGHT_PX);

        addInfo(R.string.si_SCREEN_WIDTH_DP_ID,
                String.valueOf(Math.round(displayMetrics.widthPixels / displayMetrics.density)),
                R.string.si_SCREEN_WIDTH_DP);
        addInfo(R.string.si_SCREEN_HEIGHT_DP_ID,
                String.valueOf(Math.round(displayMetrics.heightPixels / displayMetrics.density)),
                R.string.si_SCREEN_HEIGHT_DP);

        String screenDPI = displayMetrics.densityDpi + " ";
        String screenDensity;
        switch (displayMetrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                screenDPI += "LDPI";
                screenDensity = "LDPI";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                screenDPI += "MDPI";
                screenDensity = "MDPI";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                screenDPI += "HDPI";
                screenDensity = "HDPI";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                screenDPI += "XHDPI";
                screenDensity = "XHDPI";
                break;
            default:
                screenDPI = displayMetrics.densityDpi + " (Abbreviation Not supported)";
                screenDensity = "(Abbreviation Not supported)";
        }

        addInfo(R.string.si_SCREEN_DENSITY_CLASS_ID, screenDPI, R.string.si_SCREEN_DENSITY_CLASS);
        addInfo(R.string.si_SCREEN_DENSITY_ID, screenDensity, R.string.si_SCREEN_DENSITY);

        addInfo(R.string.si_SCREEN_SCALED_DENSITY_ID, String.valueOf(displayMetrics.scaledDensity),
                R.string.si_SCREEN_SCALED_DENSITY);
        addInfo(R.string.si_SCREEN_LOGICAL_DENSITY_ID, String.valueOf(displayMetrics.density),
                R.string.si_SCREEN_LOGICAL_DENSITY);
        // Report display xdpi as device's dpi
        addInfo(R.string.si_SCREEN_DPI_ID, String.format("%.0f", displayMetrics.xdpi),
                R.string.si_SCREEN_DPI);
        addInfo(R.string.si_SCREEN_X_DPI_ID, String.valueOf(displayMetrics.xdpi),
                R.string.si_SCREEN_X_DPI);
        addInfo(R.string.si_SCREEN_Y_DPI_ID, String.valueOf(displayMetrics.ydpi),
                R.string.si_SCREEN_Y_DPI);

        String curRoration = "";
        int rotation = display.getRotation();
        switch (rotation) {
            case (Surface.ROTATION_0):
                curRoration = "ROTATION_0";
                break;
            case (Surface.ROTATION_90):
                curRoration = "ROTATION_90";
                break;
            case (Surface.ROTATION_180):
                curRoration = "ROTATION_180";
                break;
            case (Surface.ROTATION_270):
                curRoration = "ROTATION_270";
                break;
            default:
                curRoration = rotation + " (Not Supported)";
        }

        addInfo(R.string.si_SCREEN_ROTATION_DEGREES_ID, curRoration,
                R.string.si_SCREEN_ROTATION_DEGREES);

        String curOrientation = "";
        switch (config.orientation) {
            case (Configuration.ORIENTATION_LANDSCAPE):
                curOrientation = "Landscape";
                break;
            case (Configuration.ORIENTATION_PORTRAIT):
                curOrientation = "Portrait";
                break;
            case (Configuration.ORIENTATION_SQUARE):
                curOrientation = "Square";
                break;
            case (Configuration.ORIENTATION_UNDEFINED):
                curOrientation = "Undefined";
                break;
            default:
                curOrientation = config.orientation + " (Not Supported)";
        }

        addInfo(R.string.si_SCREEN_ORIENTATION_ID, curOrientation, R.string.si_SCREEN_ORIENTATION);

        switch (config.orientation) {
            case (Configuration.ORIENTATION_LANDSCAPE):
                NATURAL_ORIENTATION = (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) ? Configuration.ORIENTATION_LANDSCAPE
                        : Configuration.ORIENTATION_PORTRAIT;
                break;
            case (Configuration.ORIENTATION_PORTRAIT):
                NATURAL_ORIENTATION = (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) ? Configuration.ORIENTATION_PORTRAIT
                        : Configuration.ORIENTATION_LANDSCAPE;
                break;
            case (Configuration.ORIENTATION_SQUARE):
                NATURAL_ORIENTATION = Configuration.ORIENTATION_SQUARE;
                break;
            case (Configuration.ORIENTATION_UNDEFINED):
                NATURAL_ORIENTATION = Configuration.ORIENTATION_UNDEFINED;
                break;
        }

        String natOrientation = "";
        switch (NATURAL_ORIENTATION) {
            case (Configuration.ORIENTATION_LANDSCAPE):
                natOrientation = "Landscape";
                break;
            case (Configuration.ORIENTATION_PORTRAIT):
                natOrientation = "Portrait";
                break;
            case (Configuration.ORIENTATION_SQUARE):
                natOrientation = "Square";
                break;
            case (Configuration.ORIENTATION_UNDEFINED):
                natOrientation = "Undefined";
                break;
            default:
                natOrientation = config.orientation + " (Not Supported)";
        }

        addInfo(R.string.si_SCREEN_NATURAL_ORIENTATION_ID, natOrientation,
                R.string.si_SCREEN_NATURAL_ORIENTATION);

        long diagonalPixels = Math.round(Math.sqrt(displayMetrics.widthPixels
                * displayMetrics.widthPixels + displayMetrics.heightPixels
                * displayMetrics.heightPixels));
        double x = Math.pow(displayMetrics.widthPixels / displayMetrics.xdpi, 2);
        double y = Math.pow(displayMetrics.heightPixels / displayMetrics.ydpi, 2);
        double diagonalInches = Math.sqrt(x + y);
        long diagonalMM = Math.round(diagonalInches * 25.4);

        addInfo(R.string.si_SCREEN_DIAGONAL_PX_ID, String.valueOf(diagonalPixels),
                R.string.si_SCREEN_DIAGONAL_PX);
        addInfo(R.string.si_SCREEN_DIAGONAL_INCH_ID, String.format("%.2f", diagonalInches),
                R.string.si_SCREEN_DIAGONAL_INCH);
        addInfo(R.string.si_SCREEN_DIAGONAL_MM_ID, String.valueOf(diagonalMM),
                R.string.si_SCREEN_DIAGONAL_MM);

        addInfo(R.string.si_SCREEN_REFRESH_RATE_ID, Float.toString(display.getRefreshRate()),
                R.string.si_SCREEN_REFRESH_RATE);

        String touchscreen = "";
        switch (config.touchscreen) {
            case Configuration.TOUCHSCREEN_FINGER:
                touchscreen = "Finger";
                break;
            case Configuration.TOUCHSCREEN_NOTOUCH:
                touchscreen = "No Touch";
                break;
            case Configuration.TOUCHSCREEN_STYLUS:
                touchscreen = "Stylus";
                break;
            case Configuration.TOUCHSCREEN_UNDEFINED:
                touchscreen = "Undefined";
                break;
            default:
                touchscreen = config.touchscreen + " (Not Supported)";
        }

        addInfo(R.string.si_SCREEN_TOUCHSCREEN_ID, touchscreen, R.string.si_SCREEN_TOUCHSCREEN);

    }

    public int getNaturalOrientation() {
        return NATURAL_ORIENTATION;
    }

}


package com.deviceinfo.util;

import android.os.Environment;
import android.os.StatFs;

import com.device.info.R;

public class StorageInfoUtil extends AbstractInfoUtil {

    private static StorageInfoUtil instance = null;

    public static StorageInfoUtil getInstance() {
        if (instance == null) {
            instance = new StorageInfoUtil();
        }
        return instance;
    }

    private StorageInfoUtil() {
    }

    protected void initIDs() {
        String rootDir = Environment.getRootDirectory().getAbsolutePath();
        // Gets the Android root directory.
        addInfo(R.string.sti_ROOT_DIR_ID, rootDir, R.string.sti_ROOT_DIR);

        String dataDir = Environment.getDataDirectory().getAbsolutePath();
        // Gets the Android data directory.
        addInfo(R.string.sti_DATA_DIR_ID, dataDir, R.string.sti_DATA_DIR);

        String extStorageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        // Gets the Android external storage directory.
        addInfo(R.string.sti_EXTERNAL_DIR_ID, extStorageDir, R.string.sti_EXTERNAL_DIR);

        String dlCacheDir = Environment.getDownloadCacheDirectory().getAbsolutePath();
        // Gets the Android Download/Cache content directory.
        addInfo(R.string.sti_DOWNLOAD_CACHE_DIR_ID, dlCacheDir, R.string.sti_DOWNLOAD_CACHE_DIR);

        String extStorageRemovable = Environment.isExternalStorageRemovable() ? "Yes" : "No";
        // Returns whether the primary "external" storage device is removable. If true is returned,
        // this device is for example an SD card that the user can remove. If false is returned, the
        // storage is built into the device and can not be physically removed.
        addInfo(R.string.sti_EXTERNAL_STORAGE_REMOVABLE_ID, extStorageRemovable,
                R.string.sti_EXTERNAL_STORAGE_REMOVABLE);

        StatFs stat;
        if (rootDir != null && !rootDir.isEmpty()) {
            try {
                stat = new StatFs(rootDir);
                addInfo(R.string.sti_INTERNAL_SIZE_TOTAL_MB_ID, getTotalMemory(stat),
                        R.string.sti_INTERNAL_SIZE_TOTAL_MB);
                addInfo(R.string.sti_INTERNAL_SIZE_FREE_MB_ID, getFreeMemory(stat),
                        R.string.sti_INTERNAL_SIZE_FREE_MB);
            } catch (Exception e) {
                addInfo(R.string.sti_INTERNAL_SIZE_TOTAL_MB_ID, "Can't detect",
                        R.string.sti_INTERNAL_SIZE_TOTAL_MB);
                addInfo(R.string.sti_INTERNAL_SIZE_FREE_MB_ID, "Can't detect",
                        R.string.sti_INTERNAL_SIZE_FREE_MB);
            }
        }

        if (extStorageDir != null && !extStorageDir.isEmpty()) {
            stat = null;
            try {
                stat = new StatFs(extStorageDir);
            } catch (Exception e) {
                extStorageDir = EXT_SD_PATH;
                try {
                    stat = new StatFs(extStorageDir);
                } catch (Exception ex) {

                }
            }

            if (stat != null) {
                addInfo(R.string.sti_EXTERNAL_SIZE_TOTAL_MB_ID, getTotalMemory(stat),
                        R.string.sti_EXTERNAL_SIZE_TOTAL_MB);
                addInfo(R.string.sti_EXTERNAL_SIZE_FREE_MB_ID, getFreeMemory(stat),
                        R.string.sti_EXTERNAL_SIZE_FREE_MB);

            } else {
                addInfo(R.string.sti_EXTERNAL_SIZE_TOTAL_MB_ID, "Can't detect",
                        R.string.sti_EXTERNAL_SIZE_TOTAL_MB);
                addInfo(R.string.sti_EXTERNAL_SIZE_FREE_MB_ID, "Can't detect",
                        R.string.sti_EXTERNAL_SIZE_FREE_MB);
            }

        }
    }

    private final String EXT_SD_PATH = "/mnt/sdcard/external_sd"; // actual external SD card path

    private final int MB = 1048576; // 1024*1024

    private String getTotalMemory(StatFs stat) {
        long total = ((long)stat.getBlockCount() * (long)stat.getBlockSize()) / MB;
        return Long.toString(total);
    }

    private String getFreeMemory(StatFs stat) {
        long free = ((long)stat.getAvailableBlocks() * (long)stat.getBlockSize()) / MB;
        return Long.toString(free);
    }

}

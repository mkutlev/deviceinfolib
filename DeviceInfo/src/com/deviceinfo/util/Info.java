
package com.deviceinfo.util;

/**
 * Simple class which hold the information for a parameter of the device.
 * It keeps id (name of the parameter), value and description of the parameter.
 */
public class Info {

    private String id;

    private String value;

    private String description;

    public Info(String id, String value, String description) {
        super();
        this.id = id;
        this.value = value;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}
